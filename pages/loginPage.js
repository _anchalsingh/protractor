let baseProperty = require('../testdata/baseProperty.json')
let homepage = function () {
    let usernameField = element(by.model('Auth.user.name'));
    let passwordField = element(by.model('Auth.user.password'));
    let userDescriptionField = element(by.model('model[options.key]'));
    let loginButton = element(by.cssContainingText('.btn-danger', 'Login'))
    let invalidUserAlert = element(by.css("[ng-if='Auth.error']"));
    let loggedInText = element(by.xpath('//p[.="You\'re logged in!!"]'));

    this.get = function (url) {
        browser.get(url);
    };

    this.enterUsername = function (username) {
        usernameField.sendKeys(username);
    };

    this.enterPassword = function (password) {
        passwordField.sendKeys(password);
    };

    this.enterDescription = function (userDescription) {
        userDescriptionField.sendKeys(userDescription);
    };

    this.clickOnLoginBtn = function () {
        loginButton.click();
    };

    this.verifyInvalidAlert = function () {
        expect(invalidUserAlert.getText()).toEqual(baseProperty.invalidUserMessage);
    };

    this.verifySuccessLogin = function () {
        expect(loggedInText.getText()).toEqual(baseProperty.loggedInMessage);
    };
};
module.exports = new homepage();