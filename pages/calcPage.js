let firstNumber = element(by.model('first'));
let secondNumber = element(by.model('second'));
let goButton = element(by.id('gobutton'));
let resultField = element(by.className('ng-binding'));
let history = element.all(by.repeater('result in memory'));
let gridSectionField = element.all(by.css('.table th'));
let timeSection = element(by.css("tr[ng-repeat='result in memory'] > td:nth-of-type(1)"));
let expressionSection = element.all(by.css("tr[ng-repeat='result in memory'] > td:nth-of-type(2)"));
let resultSection = element.all(by.css("tr[ng-repeat='result in memory'] > td:nth-of-type(3)"));
let addOperator = element(by.model('operator')).element(by.css("option[value='ADDITION']"));
let divideOperator = element(by.model('operator')).element(by.css("option[value='DIVISION']"));
let subtractOperator = element(by.model('operator')).element(by.css("option[value='SUBTRACTION']"));
let multiplyOperator = element(by.model('operator')).element(by.css("option[value='MULTIPLICATION']"));
let moduleOperator = element(by.model('operator')).element(by.css("option[value='MODULO']"));

this.enterFirstNo = function (firstNo) {
    firstNumber.clear();
    firstNumber.sendKeys(firstNo);
};
this.enterSecondNo = function (secondNo) {
    secondNumber.clear();
    secondNumber.sendKeys(secondNo);
};
this.clickOnGoButton = function () {
    goButton.click();
};
this.performOperator = function (firstNo, secondNo, operator) {
    this.enterFirstNo(firstNo);
    this.enterSecondNo(secondNo);
    switch (operator) {
        case '*':
            multiplyOperator.click();
            break;
        case '+':
            addOperator.click();
            break;
        case '-':
            subtractOperator.click();
            break;
        case '/':
            divideOperator.click();
            break;
        case '%':
            moduleOperator.click();
            break;
        default:
            console.log('Enter valid operator');
    }
    this.clickOnGoButton();
};
this.verifyOperatorResult = function (firstNo, secondNo, operator) {
    switch (operator) {
        case '*':
            expect(resultField.getText()).toEqual((firstNo * secondNo).toString());
            break;
        case '+':
            expect(resultField.getText()).toEqual((firstNo + secondNo).toString());
            break;
        case '-':
            expect(resultField.getText()).toEqual((firstNo - secondNo).toString());
            break;
        case '/':
            expect(resultField.getText()).toEqual((firstNo / secondNo).toString());
            break;
        case '%':
            expect(resultField.getText()).toEqual((firstNo % secondNo).toString());
            break;
        default:
            console.log('Enter valid operator');
    }

};
this.verifyGridSection = function () {
    expect(gridSectionField.get(0).getText()).toBe('Time');
    expect(gridSectionField.get(1).getText()).toBe('Expression');
    expect(gridSectionField.get(2).getText()).toBe('Result');
};
this.verifyTimeSection = function () {
    var time = new Date();
    var hours = time.getHours() % 12 || 12;
    systemTime = hours + ":" + time.getMinutes();
    expect(timeSection.getText()).toContain(systemTime);
};
this.verifyExpressionValues = function (firstNo, secondNo, operator) {
    let expectedStr;
    switch (operator) {
        case '*':
            expectedStr = firstNo + ' * ' + secondNo;
            break;
        case '+':
            expectedStr = firstNo + ' + ' + secondNo;

            break;
        case '-':
            expectedStr = firstNo + ' - ' + secondNo;
            break;
        case '/':
            expectedStr = firstNo + ' / ' + secondNo;
            break;
        case '%':
            expectedStr = firstNo + ' % ' + secondNo;
            break;
        default:
            console.log('Enter valid operator');
    }
    expect(expressionSection.get(0).getText()).toContain(expectedStr);
};
this.verifyResultSection = function (firstNo, secondNo, operator) {
    switch (operator) {
        case '*':
            expect(resultSection.get(0).getText()).toEqual((firstNo * secondNo).toString());
            break;
        case '+':
            expect(resultSection.get(0).getText()).toEqual((firstNo + secondNo).toString());
            break;
        case '-':
            expect(resultSection.get(0).getText()).toEqual((firstNo - secondNo).toString());
            break;
        case '/':
            expect(resultSection.get(0).getText()).toEqual((firstNo / secondNo).toString());
            break;
        case '%':
            expect(resultSection.get(0).getText()).toEqual((firstNo % secondNo).toString());
            break;
        default:
            console.log('Enter valid operator');
    }
};
this.getHistorySize = function () {
    return resultSection.count()
};