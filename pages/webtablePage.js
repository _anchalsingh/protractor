
let addUserButton = element(by.css("[ng-click='pop()'][type='add']"));
let firstNameText = element(by.css("[type='text'][name='FirstName']"));
let lastNameText = element(by.name('LastName'));
let usernameText = element(by.name('UserName'));
let passwordField = element(by.name('Password'));
let companyAAARadio = element(by.css("[name='optionsRadios'][value='15']"));
let companyBBBRadio = element(by.css("[name='optionsRadios'][value='16']"));
let saleOption = element(by.css("[name='RoleId']")).element(by.css("option[value='0']"));
let customerOption = element(by.css("[name='RoleId']")).element(by.css("option[value='1']"));
let adminOption = element(by.css("[name='RoleId']")).element(by.css("option[value='2']"));
let emailText = element(by.css("[name='Email']"));
let cellPhoneText = element(by.css("[name='Mobilephone']"));
let saveButton = element(by.css("[ng-click='save(user)']"));
let closeButton = element(by.css("[ng-click='close()']"));
let editBtn = element.all(by.css("[ng-click='pop()'][type='edit']")).first();
let rowList = element.all(by.repeater('dataRow in displayedCollection'));
let deleteButton = element.all(by.css("[ng-click='delUser()']")).first();
let deleteOkBtn = element(by.buttonText('OK'));
let deleteCancelBtn = element(by.buttonText('Cancel'));
let searchText = element(by.model('searchValue'));
let headerList = element.all(by.css("[class='smart-table-header-row']")).$$('th');
let lockCheckBox = element(by.css("[class='ng-pristine ng-valid'][name='IsLocked']"));
let pageNumber2 = element(by.xpath("//a[text()='2']"));



this.clickOnAddUserBtn = function () {
    addUserButton.click();
};
this.verifyWebTablePage = function () {
    expect(addUserButton.isDisplayed()).toBe(true);
    expect(editBtn.isDisplayed()).toBe(true);
}
this.verifyFormTab = function () {
    expect(firstNameText.isDisplayed()).toBe(true);
    expect(lastNameText.isDisplayed()).toBe(true);
    expect(usernameText.isDisplayed()).toBe(true);
    expect(passwordField.isDisplayed()).toBe(true);
    expect(companyAAARadio.isDisplayed()).toBe(true);
    expect(saveButton.isDisplayed()).toBe(true);
};
this.enterFirstName = function (fisrtName) {
    firstNameText.clear();
    firstNameText.sendKeys(fisrtName);
};
this.enterLastName = function (lastName) {
    lastNameText.clear();
    lastNameText.sendKeys(lastName);
};
this.enterUsername = function (username) {
    usernameText.clear();
    usernameText.sendKeys(username);
};
this.enterPassword = function (password) {
    passwordField.clear();
    passwordField.sendKeys(password);
};
this.selectCustomer = function (company) {
    switch (company) {
        case 'AAA':
            companyAAARadio.click();
            companyAAARadio.isSelected().then(radioBtnSelected => {
                console.log("Is company AAA radio selected. " + radioBtnSelected);
            });
            break;
        case 'BBB':
            companyBBBRadio.click();
            companyBBBRadio.isSelected().then(radioBtnSelected => {
                console.log("Is company BBB radio selected : " + radioBtnSelected)
            });
            break;
        default:
            this.console.log(company + " option is not available");
    }
};
this.selectRoleOption = function (option) {
    switch (option) {
        case 'sale':
            saleOption.click();
            break;
        case 'customer':
            customerOption.click();
            break;
        case 'admin':
            adminOption.click();
            break;
        default:
            console.log(option + ' is not available');
    }
};
this.enterEmail = function (email) {
    emailText.clear();
    emailText.sendKeys(email);
};
this.enterCellphon = function (mobileNo) {
    cellPhoneText.clear();
    cellPhoneText.sendKeys(mobileNo);
};
this.clickOnLockBtn = function () {
    lockCheckBox.click();
};
this.clickOnSaveBtn = function () {
    saveButton.click();
};
this.clickOnCloseBtn = function () {
    closeButton.click();
};
this.clickOnEdit = function () {
    editBtn.click();
};
this.enterUserDetails = function (fname, lname, un, pwd, company, role, email, mobile) {
    this.enterFirstName(fname);
    this.enterLastName(lname);
    this.enterUsername(un);
    this.enterPassword(pwd);
    this.selectCustomer(company);
    this.selectRoleOption(role);
    this.enterEmail(email);
    this.enterCellphon(mobile);
};
this.clickOnDelete = function (deleteUser) {
    deleteButton.click();
    if (deleteUser) {
        deleteOkBtn.click();
    } else {
        deleteCancelBtn.click();
    };
};
this.verifyUserInList = function (username) {
    var foundit = false;
    let cells = rowList.$$('td');
    cells.each(function (name) {
        name.getText().then(function (it) {
            if (it == username) {
                foundit = true;
            }
        });
    });
    return foundit;
};
this.searchText = function (searchKey) {
    searchText.sendKeys(searchKey);
};
this.verifySearchResult = function (searchKey) {
    rowList.each(function (name) {
        elem = name.$$('td').first();
        elem.getText().then(function (it) {
            console.log(it)
            expect(it).toContain(searchKey);
        });
    });
};
this.verifyHeaderValues = function () {
    expect(headerList.get(0).getText()).toBe('First Name');
    expect(headerList.get(1).getText()).toBe('Last Name');
    expect(headerList.get(2).getText()).toBe('User Name');
    expect(headerList.get(4).getText()).toBe('Customer');
    expect(headerList.get(5).getText()).toBe('Role');
    expect(headerList.get(6).getText()).toBe('E-mail');
    expect(headerList.get(7).getText()).toBe('Cell Phone');
    expect(headerList.get(8).getText()).toBe('Locked');
};
this.verifyPagination = function () {
    expect(pageNumber2.isDisplayed()).toBe(true);
};
this.veryGood = function(){
    rowList.each(function (name) {
        elem = name.$$('td').first();
        elem.getText().then(function (it) {
            console.log(it)
            expect(it).toContain(searchKey);
        });
    });
}
