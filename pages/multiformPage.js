let baseProperty = require('../testdata/baseProperty')

let nameField = element(by.model('formData.name'));
let emailField = element(by.model('formData.email'));
let profileField = element(by.css("[ui-sref='.profile']"));
let nextToInterestBtn = element(by.css("[ui-sref='form.interests']"));
let nextToPaymentBtn = element(by.css("[ui-sref='form.payment']"));
let jsonTextbox = element(by.xpath("//pre[@class='ng-binding']"));
let interestField = element(by.css("[ui-sref='.interests']"));
let paymentField = element(by.css("[ui-sref='.payment']"));
let psRadio = element(by.css("[value='ps']"));
let xboxRadio = element(by.xpath("//label[contains(.,'I like XBOX')]"));
let submitBtn = element(by.xpath("//button[@class='btn btn-danger']"));
let completeField = element(by.xpath("//*[@id='form-views']/div/h3"));

this.get = function (url) {
    browser.get(url);
};

this.getFormData = function () {
    return jsonTextbox.getText();
};
this.verifyProfilePage = function () {
    expect(emailField.isDisplayed()).toBe(true);
    expect(nextToInterestBtn.isDisplayed()).toBe(true);
};
this.clickOnProfile = function () {
    profileField.click();
};
this.verifyProfileIsActive = function () {
    expect(profileField.getAttribute('class')).toEqual('active');
};

this.enterName = function (name) {
    nameField.clear();
    nameField.sendKeys(name);
};

this.enterEmail = function (email) {
    emailField.clear();
    emailField.sendKeys(email);
};

this.clickOnNextToInterest = function () {
    nextToInterestBtn.click();
};
this.clickOnInterestPage = function () {
    interestField.click();
};
this.verifyInterestIsActive = function () {
    expect(interestField.getAttribute('class')).toEqual('active');
};

this.clickOnPsRadio = function () {
    psRadio.click();
};
this.clickOnPaymentPage = function () {
    paymentField.click();
};
this.clickOnNextToPaymnet = function () {
    nextToPaymentBtn.click();
};
this.verifyInterestPage = function () {
    expect(psRadio.isDisplayed()).toBe(true);
    expect(xboxRadio.isDisplayed()).toBe(true);
};
this.verifyPaymentPage = function () {
    expect(completeField.isDisplayed()).toBe(true);
    expect(submitBtn.isDisplayed()).toBe(true);
};
this.verifyPaymentIsActive = function () {
    expect(paymentField.getAttribute('class')).toEqual('active');
};

this.clickOnSubmitBtn = function () {
    submitBtn.click();
};

this.verifyCompletedMsg = function () {
    expect(completeField.getText()).toEqual(baseProperty.completedMessage);
};

this.verifyAlert = function () {
    browser.switchTo().alert().then((alert) => {
        expect(alert.getText()).toEqual('awesome!')
        alert.accept();
    });
};