baseProperty = require('../testdata/baseProperty.json')
webtablePage = require('../pages/webtablePage')
user = require('../testdata/webTableData.json')
describe('Verify Webtable page', function () {
    beforeEach(function () {
        browser.get(baseProperty.webtableURL);
        browser.driver.manage().window().maximize();
    });
    it('Task6(a): Add User and validate added user in the grid', function () {
        let fname = user.fname;
        let lname = user.lname;
        let username = user.username;
        let pwd = user.password;
        let company = user.companyB;
        let role = user.sale;
        let email = user.email;
        let mobileNo = user.mobileNo;

        webtablePage.clickOnAddUserBtn();
        webtablePage.verifyFormTab();
        webtablePage.enterUserDetails(fname, lname, username, pwd, company, role, email, mobileNo);
        webtablePage.clickOnSaveBtn();
        webtablePage.verifyUserInList(fname);
    });

    it('Task6(b): Verify Edit record and validate successful edit', function () {
        let fname = user.fname;
        let lname = user.lname;

        webtablePage.clickOnEdit();
        webtablePage.verifyFormTab();
        webtablePage.enterFirstName(fname);
        webtablePage.enterLastName(lname);
        webtablePage.clickOnSaveBtn();
        webtablePage.verifyWebTablePage();
        let userPresent = webtablePage.verifyUserInList(fname);
        if (userPresent) {
            console.log(fname + ' : is edited successfuly');
        } else {
            console.log(fname + ' : is not edited');
        };
    });

    it('Task6(c): Verify delete button and validate successful delete', function () {
        let name = user.name;
        webtablePage.clickOnDelete(true);
        expect(webtablePage.verifyUserInList(name)).toBe(false);
        console.log('User is deleted from the list');
    });

    it('Task6(d): Validate search field and respective result', function () {
        let name = user.name;
        webtablePage.verifyWebTablePage();
        webtablePage.searchText(name);
        webtablePage.verifySearchResult(name);
    });

    it('Task6(e): Verify header grid', function () {
        webtablePage.verifyWebTablePage();
        webtablePage.verifyHeaderValues();
    });
    //Task6(f) is not implemented due to logical issue 

    it('Task6(g): Verify after ten entries the pagination is 2', function () {
        let fname = user.fname;
        let lname = user.lname;
        let username = user.username;
        let pwd = user.password;
        let company = user.companyB;
        let role = user.sale;
        let email = user.email;
        let mobileNo = user.mobileNo;

        for (i = 0; i <= 5; i++) {
            webtablePage.clickOnAddUserBtn();
            webtablePage.verifyFormTab();
            webtablePage.enterUserDetails(fname, lname, username, pwd, company, role, email, mobileNo);
            webtablePage.clickOnSaveBtn();
        };
        webtablePage.verifyPagination();
    });
});