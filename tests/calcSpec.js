let baseProperty = require('../testdata/baseProperty');
let calcPage = require('../pages/calcPage')
describe("Verifying Calc page in way2automation.com", function () {
    beforeEach(function () {
        browser.get(baseProperty.calcURL);
        browser.driver.manage().window().maximize();
    });
    it('Verify Addition operation and grid sections', function () {
        //task 4(a)
        calcPage.performOperator(5, 2, baseProperty.add);
        calcPage.verifyOperatorResult(5, 2, baseProperty.add);
        //task 4(b) and 4(f)
        calcPage.verifyResultSection(5, 2, baseProperty.add);
        //task 4(c)
        calcPage.verifyGridSection();
        //task 4(d)
        calcPage.verifyTimeSection();
        //task 4(e)
        calcPage.verifyExpressionValues(5, 2, baseProperty.add);
    });
    it('five iterations and validate the grid size should be 5', function () {
        //task 4(g)
        calcPage.performOperator(3, 4, baseProperty.add);
        calcPage.performOperator(6, 4, baseProperty.subtract);
        calcPage.performOperator(1, 4, baseProperty.multiply);
        calcPage.performOperator(2, 4, baseProperty.multiply);
        calcPage.performOperator(4, 4, baseProperty.divide);
        expect(calcPage.getHistorySize()).toEqual(baseProperty.historySize);
    });
    it('Verify Subtract operation and grid sections', function () {
        //task 4(h) for subtraction
        calcPage.performOperator(5, 2, baseProperty.subtract);
        calcPage.verifyOperatorResult(5, 2, baseProperty.subtract);
        calcPage.verifyGridSection();
        calcPage.verifyTimeSection();
        calcPage.verifyExpressionValues(5, 2, baseProperty.subtract);
        calcPage.verifyResultSection(5, 2, baseProperty.subtract);
    });
    it('Verify multiply operation and grid sections', function () {
        //task 4(h) for multiply
        calcPage.performOperator(5, 2, baseProperty.multiply);
        calcPage.verifyOperatorResult(5, 2, baseProperty.multiply);
        calcPage.verifyGridSection();
        calcPage.verifyTimeSection();
        calcPage.verifyExpressionValues(5, 2, baseProperty.multiply);
        calcPage.verifyResultSection(5, 2, baseProperty.multiply);
    });
    it('Verify divide operation and grid sections', function () {
        //task 4(h) for divide
        calcPage.performOperator(6, 2, baseProperty.divide);
        calcPage.verifyOperatorResult(6, 2, baseProperty.divide);
        calcPage.verifyGridSection();
        calcPage.verifyTimeSection();
        calcPage.verifyExpressionValues(6, 2, baseProperty.divide);
        calcPage.verifyResultSection(6, 2, baseProperty.divide);
    });
    it('Verify modulo operation and grid sections', function () {
        //task 4(h) for modulo
        calcPage.performOperator(6, 2, baseProperty.modulo);
        calcPage.verifyOperatorResult(6, 2, baseProperty.modulo);
        calcPage.verifyGridSection();
        calcPage.verifyTimeSection();
        calcPage.verifyExpressionValues(6, 2, baseProperty.modulo);
        calcPage.verifyResultSection(6, 2, baseProperty.modulo);
    });
});