let multiform = require('../pages/multiformPage');
let baseProperty = require('../testdata/baseProperty');
let multiformData = require('../testdata/multiformData');
var using = require('jasmine-data-provider');
describe('Verify multiform page in way2automation', function () {
    beforeEach(function () {
        multiform.get(baseProperty.multiformURL);
        browser.driver.manage().window().maximize();
    });

    it('Varify input is displayed in json format ', function () {
        multiform.enterName(baseProperty.name);
        multiform.enterEmail(baseProperty.email);
        // task 3(a)
        multiform.getFormData().then(function (text) {
            textData = JSON.parse(text);
            expect(textData['name']).toEqual(baseProperty.name);
            expect(textData['email']).toEqual(baseProperty.email);
        });
    });

    it('Verify Interest Screen', function () {
        multiform.enterName(baseProperty.name);
        multiform.enterEmail(baseProperty.email);
        //task 3(b)
        multiform.clickOnNextToInterest();
        //task 3(c)
        multiform.verifyInterestIsActive();
    });

    it('Verify radio and Payment Screen', function () {
        multiform.enterName(baseProperty.name);
        multiform.enterEmail(baseProperty.email);
        multiform.clickOnNextToInterest();
        multiform.verifyInterestPage();
        //task 3(d)
        multiform.clickOnPsRadio();
        multiform.getFormData().then(function (text) {
            textData = JSON.parse(text);
            expect(textData['type']).toEqual(baseProperty.type);
        });
        //task 3(e)
        multiform.clickOnNextToPaymnet();
        //task 3(f)
        multiform.verifyPaymentIsActive();
        //task 3(g)
        multiform.verifyCompletedMsg();
        //task 3(h)
        multiform.clickOnSubmitBtn();
        multiform.verifyAlert();
    });

    it('validate successful landing on respective pages', function () {
        multiform.clickOnPaymentPage();
        multiform.verifyPaymentPage();
        multiform.verifyPaymentIsActive();
        multiform.clickOnInterestPage();
        multiform.verifyInterestPage();
        multiform.verifyInterestIsActive();
        //task 3(i)
        multiform.clickOnProfile();
        multiform.verifyProfilePage();
        multiform.verifyProfileIsActive();
    });
    using(multiformData.details, function (data) {
        it('Verifying multiform profile page with: ' + data.description, function () {
            //task 3(j)
            multiform.verifyProfilePage();
            multiform.enterName(data.name);
            multiform.enterEmail(data.email);
            multiform.clickOnNextToInterest();
            multiform.verifyInterestPage();
            multiform.clickOnPsRadio();
            multiform.clickOnPaymentPage();
            multiform.verifyPaymentPage();
        });
    });
});