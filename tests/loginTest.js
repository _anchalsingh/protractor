let homepage = require('../pages/loginPage');
var fetchedUser = require('../testdata/loginData.js');
var using = require('jasmine-data-provider');
var baseProperty = require('../testdata/baseProperty.json')

describe('Verify Login on Way2automation', function () {
    beforeEach(function () {
        homepage.get(baseProperty.loginURL);
    });
    it("Verify successfully login", function () {
        homepage.enterUsername(baseProperty.username);
        homepage.enterPassword(baseProperty.password);
        homepage.enterDescription(baseProperty.description);
        homepage.clickOnLoginBtn();
        homepage.verifySuccessLogin();
    });

    using(fetchedUser.userdetails, function (data) {
        it("Verify invalid login", function () {
            homepage.enterUsername(data.username);
            homepage.enterPassword(data.password);
            homepage.enterDescription(data.description);
            homepage.clickOnLoginBtn();
            homepage.verifyInvalidAlert();
        });
    });
});
