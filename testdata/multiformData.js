'use strict';
module.exports = {
    details: {
        'user1': { name: 'mark cyrus', email: 'Test@gmail.com', description: 'Valid name and email' },
        'user2': { name: 'Test test', email: 'Tesaddsd', description: 'Valid name and invalid email' },
        'user3': { name: '#@$%^&*', email: 'Test@gmail.com', description: 'Special character name and valid email' },
        'user4': { name: '#@$%^&*', email: '#@$%^&*', description: 'Invalid name and email' },
        'user5': { name: '', email: '#@$%^&*', description: 'special character in email field' },
        'user6': { name: '#@$%^&*', email: '', description: 'special character in name field' },
        'user7': { name: '', email: '', description: 'blank name and email field' }
    }
}