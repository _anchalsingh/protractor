'use strict';
module.exports = {
    userdetails: {
        'user1': { username: 'anchal.singh', password: 'Test@132', description: 'Invalid userename and invalid password' },
        'user2': { username: 'protractor.test', password: 'Tesaddsd', description: 'Invalid userename and valid password' },
        'user3': { username: 'NodeJS', password: 'Testwerer', description: 'Valid userename and invalid password' },
        'user4': { username: 'invalid.test', password: 'Tesfretgst@132', description: 'Valid userename, passowr' }
    }
}